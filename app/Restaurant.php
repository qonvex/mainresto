<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Restaurant extends Model
{
    public function shop()
    {
        return $this->hasMany('App\Shop', 'id_restaurant', 'id');
    }
}
