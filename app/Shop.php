<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Shop extends Model
{
	public function restaurant()
	{
	    return $this->belongsTo('App\Restaurant', 'id', 'id_restaurant');
	}
}
