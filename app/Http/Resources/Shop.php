<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\Restaurant;
use App\Restaurant as Resto;

class Shop extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        // return parent::toArray($request);
        return [
            'id' => $this->id,
            'id_restaurant' => $this->id_restaurant,
            'name' => $this->name,
            'photo' => 'https://upload.wikimedia.org/wikipedia/commons/thumb/3/36/McDonald%27s_Golden_Arches.svg/1200px-McDonald%27s_Golden_Arches.svg.png',
            'address' => 'XYZ bldg. Calbayog City',
            'restaurant' => new Restaurant(Resto::find($this->id_restaurant))
        ];
    }
}
