<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        DB::table('restaurants')->insert([
            'token' => 1,
            'domain' => 'resto.ohm-conception.com',
            'name' => 'OHM Restor',
        ]);

        DB::table('shops')->insert([
            'id_restaurant' => 1,
            'name' => 'MCdo Dagum',
        ]);

        DB::table('shops')->insert([
            'id_restaurant' => 1,
            'name' => 'Jolliboy Capoocan',
        ]);

        DB::table('shops')->insert([
            'id_restaurant' => 1,
            'name' => 'MCdo Obrero',
        ]);
    }
}
