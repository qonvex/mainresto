<?php

use Illuminate\Database\Seeder;

class RestaurantsTableSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        DB::table('restaurants')->insert([
            'token' => 1,
            'domain' => 'resto.ohm-conception.com',
            'name' => 'OHM Restor',
        ]);
    }
}
