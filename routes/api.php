<?php

use Illuminate\Http\Request;
use App\Shop;
use App\Restaurant;
use App\Http\Resources\ShopCollection;
use App\Http\Resources\RestaurantCollection;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('shop/search/{shopname}', function ($shopname = "") {
    return new ShopCollection(Shop::where('name', 'like', "%" . $shopname . "%")->get());
});
